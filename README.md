# Signature-Chain

A Blockchain, that uses neither Proof of Stake nor Proof of Work,
but a Signature Trust based Branching Rule.

# Blocks
Fields:
1. Content CID
2. Content Signature by the Block creator
3. Signatures of the previous Block's CID by other Block creators
4. CID of the previous Block
5. Public Key of Block creator

All Blocks refer to their predecessor via
CID.

They also refer to an IPFS Content Object via
CID and store a number of Signatures.
One Signature of the content
and several Signatures of the previous block.

In case of a branching, the branch
with the most such valid signatures
is preferred.

# Proposals
Any peer can propose a block.
But they will have to gather
as many valid signatures of
the previous block by other
peers that have already made a proposal.

Because again, the branch with the most
valid signatures on them, should
be preferred.
