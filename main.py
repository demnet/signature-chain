"""
main.py Module of the PoC
implementation.
Requires a running IPFS Daemon on localhost.

usage: main.py [-h] [--init] [--run PUBSUB] cid

positional arguments:
  cid           CID to handle

optional arguments:
  -h, --help    show this help message and exit
  --init        Initialize with CID
  --run PUBSUB  Run as miner on cid with pub sub room

"""
import argparse
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
import ipfshttpclient

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("cid", help = "CID to handle")
    parser.add_argument("--init", help = "Initialize with CID", action="store_true")
    parser.add_argument("--run", metavar = "PUBSUB", help = "Run as miner on cid with pub sub room")
    args = parser.parse_args()

    with ipfshttpclient.connect() as ipfs:
        if args.init:
            init(ipfs, args.cid)

        if args.run is not None:
            run(ipfs, args.cid, args.run)

def init(ipfs,cid):
    """Initiate
    """
